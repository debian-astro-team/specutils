Source: specutils
Maintainer: Debian Astronomy Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Section: python
Priority: optional
Build-Depends: cython3,
               debhelper-compat (= 13),
               dh-python,
               python3-all-dev,
               python3-asdf (>= 2.10~),
               python3-astropy (>= 5.1~),
               python3-gwcs (>= 0.17~),
               python3-matplotlib <!nodoc> <!nocheck>,
               python3-ndcube (>= 2.0.0~),
               python3-pytest <!nocheck>,
               python3-pytest-astropy <!nocheck>,
               python3-scipy,
               python3-setuptools,
               python3-setuptools-scm,
               python3-spectral-cube <!nocheck>,
               python3-sphinx-astropy <!nodoc>
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-astro-team/specutils
Vcs-Git: https://salsa.debian.org/debian-astro-team/specutils.git
Homepage: https://github.com/astropy/specutils
Rules-Requires-Root: no

Package: python3-specutils
Architecture: any
Depends: ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Recommends: python3-scipy
Description: Base classes and utilities for astronomical spectra in Python
 The specutils package implements base classes and utilities for
 interacting with astronomical spectra in Python and the Astropy
 project. It is intended for eventual merger with the astropy package,
 but for now is being developed independently.
